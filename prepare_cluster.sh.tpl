#!/bin/bash
export HELM_EXPERIMENTAL_OCI=1
yc managed-kubernetes cluster get-credentials ${cluster_id} --external
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm upgrade --install --namespace monitoring --create-namespace prometheus-stack prometheus-community/kube-prometheus-stack
helm upgrade --install --namespace loki-stack --set grafana.enabled=true --create-namespace loki grafana/loki-stack
helm upgrade --install --namespace ingress-operator --create-namespace ingress-nginx ingress-nginx/ingress-nginx
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.1/cert-manager.yaml
#sleep 30
#kubectl apply -f grafana-ingress.yaml
