variable cloud_id {
  description = "Cloud"
}
variable folder_id {
  description = "Folder"
}
variable zone {
  description = "Zone"
  # Значение по умолчанию
  default = "ru-central1-a"
}
variable public_key {
  # Описание переменной
  description = "Path to the public key used for ssh access"
}

variable "dns_domain" {
  description = "Name of domain"
  default = "rshalgochev.ru."
}

variable service_account_key {
  description = "key.json"
}

variable private_key {
  # Описание переменной
  description = "Path to the public key used for ssh access"
}

variable "dns_domains_list" {
  type = list
  description = "Name of domain"
  default = ["rshalgochev.ru", "*.rshalgochev.ru"]
}
