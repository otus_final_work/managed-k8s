variable "network_id" {
  description = "Cluster network ID"
}

variable "zone" {
  description = "Availability zone"
  default = "ru-central1-a"
}

variable "iam_sa_id" {
  description = "YC IAM service account ID"
}

variable "subnet_id" {
  description = "Kluster subnet_id"
}

variable "cluster_version" {
  description = "K8S cluster version"
  default = "1.26"
}

variable "cluster_id" {
  description = "K8S cluster ID"
}

variable "maintenance_duration" {
  description = "Duration of maintenance window"
  default = "5h"
}

variable "maintenance_start" {
  description = "Start of maintenance window"
  default = "02:00"
}

variable "maintenance_day" {
  description = "Day of maintenance"
  default = "sunday"
}

variable "public_key" {
  description = "Public key path"
}

variable "group_size" {
  description = "Count of worker nodes"
  default = 3
}

variable "name" {
  description = "Name of nodes group"
}