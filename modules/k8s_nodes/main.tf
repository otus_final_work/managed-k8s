terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "~>0.35"
    }
  }
  required_version = ">= 0.13"
}

resource "yandex_kubernetes_node_group" "k8s_nodes_group" {
  cluster_id = var.cluster_id
  name = var.name
  version = var.cluster_version
  instance_template {
    platform_id = "standard-v2"
    network_interface {
      nat = true
      subnet_ids = [var.subnet_id]
    }
    resources {
      memory = 8
      cores = 4
    }
    boot_disk {
      type = "network-hdd"
      size = 64
    }
    metadata = {
      ssh-keys = "ubuntu:${file(var.public_key)}"
    }
    scheduling_policy {
      preemptible = false
    }
  }
  scale_policy {
    fixed_scale {
      size = var.group_size
    }
  }
  allocation_policy {
    location {
      zone = var.zone
    }
  }
  maintenance_policy {
    auto_repair  = true
    auto_upgrade = true
    maintenance_window {
      day = var.maintenance_day
      duration   = var.maintenance_duration
      start_time = var.maintenance_start
    }
  }
}
