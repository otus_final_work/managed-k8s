terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "~>0.35"
    }
  }
  required_version = ">= 0.13"
}

resource "yandex_kubernetes_cluster" "my_k8s_cluster" {
  network_id              = var.network_id
  node_service_account_id = var.iam_sa_id
  service_account_id      = var.iam_sa_id
  release_channel         = var.release_channel
  network_policy_provider = var.network_policy_provider
  master {
    version = var.cluster_version
    zonal {
      zone = var.zone
      subnet_id = var.subnet_id
    }
    public_ip = true

    maintenance_policy {
      auto_upgrade = true
      maintenance_window {
        duration   = var.maintenance_duration
        start_time = var.maintenance_start
      }
    }
  }
}