variable "network_id" {
  description = "Cluster network ID"
}

variable "zone" {
  description = "Availability zone"
  default = "ru-central1-a"
}

variable "iam_sa_id" {
  description = "YC IAM service account ID"
}

variable "subnet_id" {
  description = "Kluster subnet_id"
}

variable "cluster_version" {
  description = "K8S cluster version"
  default = "1.26"
}

variable "maintenance_duration" {
  description = "Duration of maintenance window"
  default = "5h"
}

variable "maintenance_start" {
  description = "Start of maintenance window"
  default = "02:00"
}

variable "network_policy_provider" {
  description = "Network policy provider"
  default = "CALICO"
}

variable "release_channel" {
  description = "Release channel"
  default = "RAPID"
}