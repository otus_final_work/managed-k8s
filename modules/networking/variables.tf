variable "zone" {
  description = "Availability zone"
  default = "ru-central1-a"
}

variable "network_name" {
  description = "Name of network"
  default = "k8s"
}

variable "subnet_name" {
  description = "Name of subnet"
  default = "k8s-subnet-a"
}

variable "subnet_cidr" {
  description = "Subnet CIDR"
  default = "10.0.0.0/16"
}