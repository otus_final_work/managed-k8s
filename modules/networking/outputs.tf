output "network_id" {
  value = yandex_vpc_network.k8s.id
}

output "subnet_id" {
  value = yandex_vpc_subnet.k8s-subnet-a.id
}

output "subnet_cidr_blocks" {
  value = yandex_vpc_subnet.k8s-subnet-a.v4_cidr_blocks
}