terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "~>0.35"
    }
  }
  required_version = ">= 0.13"
}

resource "yandex_vpc_network" "k8s" {
  name = var.network_name
}

resource "yandex_vpc_subnet" "k8s-subnet-a" {
  network_id     = yandex_vpc_network.k8s.id
  v4_cidr_blocks = [var.subnet_cidr]
  name = var.subnet_name
  zone = var.zone
}