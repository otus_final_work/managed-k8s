terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "~>0.35"
    }
  }
  required_version = ">= 0.13"
}

resource "yandex_vpc_security_group" "vm_group_sg" {
  network_id        = var.network_id

  ingress {
    protocol          = var.protocol
    description       = "Allow incoming traffic from members of the same security group"
    from_port         = var.start_port
    to_port           = var.end_port
    predefined_target = "self_security_group"
    v4_cidr_blocks    = var.cidr
  }
  egress {
    protocol          = var.protocol
    description       = "Allow incoming traffic from members of the same security group"
    from_port         = var.start_port
    to_port           = var.end_port
    predefined_target = "self_security_group"
    v4_cidr_blocks    = var.cidr
  }
}