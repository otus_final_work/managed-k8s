variable "protocol" {
  description = "SG protocol"
  default     = "ANY"
}

variable "start_port" {
  description = "SG start port"
  default     = 0
}

variable "end_port" {
  description = "SG end port"
  default     = 65535
}

variable "cidr" {
  description = "SG CIDR"
}

variable "network_id" {
  description = "SG Network ID"
}