terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "~>0.35"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = pathexpand(var.service_account_key)
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.zone
}

 resource "yandex_iam_service_account" "my_k8s_cluster" {
   name = "my-k8s-cluster"
   description = "Service account for my_k8s_cluster"
 }

resource "yandex_resourcemanager_folder_iam_binding" "admin" {
  folder_id = var.folder_id
  members   = ["serviceAccount:${yandex_iam_service_account.my_k8s_cluster.id}"]
  role      = "editor"
  depends_on = [yandex_iam_service_account.my_k8s_cluster]
}

module "network" {
  source = "./modules/networking"
}

module "ingress_sg" {
  source = "./modules/k8s_sg"
  protocol = "TCP"
  start_port = 10501
  end_port = 10502
  cidr = module.network.subnet_cidr_blocks
  network_id = module.network.network_id
}

module "common_sg" {
  source = "./modules/k8s_sg"
  cidr = module.network.subnet_cidr_blocks
  network_id = module.network.network_id
}

resource "yandex_dns_zone" "my-otus-zone" {
  name = "otus-zone"
  zone = var.dns_domain
  public = true
  private_networks = [module.network.network_id]
}

module "k8s_cluster" {
  source = "./modules/k8s_master"
  network_id = module.network.network_id
  iam_sa_id = yandex_iam_service_account.my_k8s_cluster.id
  subnet_id = module.network.subnet_id
  depends_on = [module.network, yandex_resourcemanager_folder_iam_binding.admin]
}

module "k8s_nodes_group" {
  source = "./modules/k8s_nodes"
  network_id = module.network.network_id
  subnet_id = module.network.subnet_id
  iam_sa_id = yandex_iam_service_account.my_k8s_cluster.id
  name = "k8s-workers"
  public_key = var.public_key
  cluster_id = module.k8s_cluster.cluster_id
  depends_on = [module.k8s_cluster]
}

locals {
  cluster_id = module.k8s_cluster.cluster_id
}

resource "local_file" "run_helm" {
  filename = "prepare_cluster.sh"
  content = templatefile("prepare_cluster.sh.tpl", {
    cluster_id = local.cluster_id
  })

  provisioner "local-exec" {
    command = "chmod +x prepare_cluster.sh"
  }

  provisioner "local-exec" {
    command = "/bin/bash prepare_cluster.sh"
    working_dir = "."
  }

  depends_on = [module.k8s_nodes_group]
}